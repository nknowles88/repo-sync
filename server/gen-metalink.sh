#!/bin/bash

set -e
readonly epel7_repomd=/opt/repos/epel/7/x86_64/repodata/repomd.xml

cat > /etc/nginx/conf.d/epel7-meta.inc << EOF
if (\$arg_repo = epel-7) {
    return 200
'<?xml version="1.0" encoding="utf-8"?>
<metalink version="3.0" xmlns="http://www.metalinker.org/" xmlns:mm0="http://fedorahosted.org/mirrormanager">
<files>
  <file name="repomd.xml">
    <mm0:timestamp>$(stat -c %Y $epel7_repomd)</mm0:timestamp>
    <size>$(stat -c %s $epel7_repomd)</size>
    <verification>
      <hash type="md5">$(md5sum $epel7_repomd | awk '{print $1}')</hash>
      <hash type="sha1">$(sha1sum $epel7_repomd | awk '{print $1}')</hash>
      <hash type="sha256">$(sha256sum $epel7_repomd | awk '{print $1}')</hash>
      <hash type="sha512">$(sha512sum $epel7_repomd | awk '{print $1}')</hash>
    </verification>
    <resources>
      <url protocol="http" type="http">http://\$server_addr/repos/epel/7/x86_64/repodata/repomd.xml</url>
    </resources>
  </file>
</files>
</metalink>
';
}
EOF
