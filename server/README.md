# File Locations

After installing nginx and named, these files should be placed at the following
locations on the offline server. If using Ubuntu the locations might be
different, but you should be able to figure it out.

| Path                     | Server path                     |
| ------------------------ | ------------------------------- |
| `gen-metalink.sh`        | `/root/`                        |
| `named/named.conf`       | `/etc/`                         |
| `named/repos.db`         | `/var/named/`                   |
| `named/repos.reverse.db` | `/var/named/`                   |
| `nginx/autoindex.inc`    | `/etc/nginx/`                   |
| `nginxtls-common.inc`    | `/etc/nginx/`                   |
| `nginx/conf.d/*`         | `/etc/nginx/conf.d/`            |
| `nginx/default.d/*`      | `/etc/nginx/default.d/`         |
| `nginx/ubuntu/*`         | `/usr/share/nginx/html/ubuntu/` |
