listen  443 ssl http2;
listen  [::]:443 ssl http2;
ssl_session_cache  shared:SSL:1m;
ssl_session_timeout  10m;
ssl_ciphers  PROFILE=SYSTEM;
ssl_prefer_server_ciphers  on;
# Need to provide these
#ssl_certificate  "/etc/pki/nginx/some.crt";
#ssl_certificate_key  "/etc/pki/nginx/private/some.key";
