#!/usr/bin/bash
# vim: ai et ts=2 sts=2 sw=2
#shellcheck disable=SC2317
#shellcheck disable=SC2329

set -e

readonly CHECK_CMDS=(createrepo_c reposync rsync zcat)
for cmd in "${CHECK_CMDS[@]}"; do
  command -v "${cmd}" &> /dev/null || {
    echo "Missing required command: ${cmd}"
    exit 1
  }
done

# v3.1.3-16 started adding --sparse-block=1024 to options sent to server, and
# the server doesn't like it.
if rsync --version | grep -q 3\.1\.3 &> /dev/null; then
  SPARSE='--sparse-block=0'
fi

for i in "$@"; do
  if [[ "$i" = -n || "$i" = --dry-run ]]; then
    DRY_RUN=-n
    break
  fi
done

readonly REPO_DIR="${PWD}/repos"
#shellcheck disable=SC2206
readonly RSYNC_FLAGS=(
  -rltmv ${DRY_RUN} --delete-before --delete-excluded ${SPARSE} --no-motd
  --exclude *.drpm
)
readonly EPEL_MIRROR='mirrors.sonic.net'
readonly UBUNTU_MIRROR='mirror.siena.edu'

#------------------------------------------------------------------------------

function usage() {
  cat <<EOF
usage: $(basename "$0") [-n|--dry-run] [OPTIONS]

Helper script to sync various linux OS repos locally for purpose of creating
an offline repo. The first time running will take a while as there is no local
cache of packages. Subsequent runs will download newer packages so only the
deltas are downloaded.

Options that sync multiple architectures will have architectures synced in [].
Currently it is not supported to download only x86_64 or arm64 pacakges for
these.

If 'all' is passed these repos will be synced:
- docker (centos 7 and 8)
- epel8 [x86_64, arm64]
- epel next (8) [x86_64, arm64]
- ubuntu 18.04
- ubuntu 20.04 [x86_64, arm64]
- ubuntu 22.04

Options (* = deprecated):
  all               See above list for what is synced
  *centos7          Only sync centos 7
  *centos8_stream   Only sync centos 8 stream [x86_64, arm64]
  *centos8          Only sync centos 8 [x86_64, arm64]
  debian            Sync the following debian repos
    debian10          Only sync debian 10
  docker            Sync the following docker repos
    docker_centos7    Only sync docker ce stable (centos 7)
    docker_centos8    Only sync docker ce stable (centos 8)
  epel              Sync the following epel repos
    epel8             Only sync epel 8 [x86_64, arm64]
    epel_next         Only sync epel next [x86_64, arm64]
  *epel7            Only sync epel 7 [x86_64, arm64]
  *redhat75         Only sync redhat 7.5
  ubuntu            Sync the following ubuntu repos
    ubuntu1804        Only sync ubuntu 18.04
    ubuntu2004        Only sync ubuntu 20.04
    ubuntu2004_arm64  Only sync ubuntu 20.04 ARM64
    ubuntu2204        Only sync ubuntu 22.04
  *ubuntu1204       Only sync ubuntu 12.04
EOF
  exit 1
}

function _print_section() {
  local section=$1
  cat <<EOF
###############################################################################
# ${section}
###############################################################################
EOF
}

function sync_centos7() {
  local addl_flags=(
    --exclude '/*/atomic/'
    --exclude '/*/centosplus/'
    --exclude '/*/cloud/'
    --exclude '/*/configmanagement/'
    --exclude '/*/cr/'
    --exclude '/*/dotnet/'
    --exclude '/*/fasttrack/'
    --exclude '/*/infra/'
    --exclude '/*/messaging/'
    --exclude '/*/nfv/'
    --exclude '/*/opstools/'
    --exclude '/*/paas/'
    --exclude '/*/rt/'
    --exclude '/*/storage/'
    --exclude '/*/virt/'
    --exclude '/*/*/Source/'
  )
  local repo='archive.kernel.org::centos-vault/7.9.2009'

  _print_section 'Syncing CentOS 7'
  mkdir -p "${REPO_DIR}/centos"
  rsync "${RSYNC_FLAGS[@]}" "${addl_flags[@]}" "${repo}" "${REPO_DIR}/centos/"
}

function sync_centos8() {
  # x86_64 and arm64
  local addl_flags=(
    --exclude '/*/Devel/'
    --exclude '/*/HighAvailability/'
    --exclude '/*/centosplus/'
    --exclude '/*/cloud/'
    --exclude '/*/configmanagement/'
    --exclude '/*/cr/'
    --exclude '/*/fasttrack/'
    --exclude '/*/infra/'
    --exclude '/*/kmods/'
    --exclude '/*/messaging/'
    --exclude '/*/nfv/'
    --exclude '/*/NFV/'
    --exclude '/*/opstools/'
    --exclude '/*/storage/'
    --exclude '/*/virt/'
    --exclude '/**/kickstart/'
    --exclude '/**/ppc64le/'
    --exclude '/**/Source/'
  )
  local repo='archive.kernel.org::centos-vault/8.5.2111'

  _print_section 'Syncing CentOS 8'
  mkdir -p "${REPO_DIR}/centos"
  rsync "${RSYNC_FLAGS[@]}" "${addl_flags[@]}" "${repo}" "${REPO_DIR}/centos/"
}

function sync_centos8_stream() {
  # x86_64 and arm64
  local addl_flags=(
    --exclude '.~tmp~'
    --exclude '/*/Devel/'
    --exclude '/*/HighAvailability/'
    --exclude '/*/NFV/'
    --exclude '/*/RT/'
    --exclude '/*/ResilientStorage/'
    --exclude '/*/aarch64/'
    --exclude '/*/centosplus/'
    --exclude '/*/cloud/'
    --exclude '/*/core/'
    --exclude '/*/cr/'
    --exclude '/*/fasttrack/'
    --exclude '/*/hyperscale/'
    --exclude '/*/isos/*/*-latest-*.iso'
    --exclude '/*/kmods/'
    --exclude '/*/messaging/'
    --exclude '/*/nfv/'
    --exclude '/*/opstools/'
    --exclude '/*/ppc64le/'
    --exclude '/*/storage/'
    --exclude '/*/virt/'
    --exclude '/*/*/Source/'
    --exclude 'ppc64le/***'
    --exclude 'EFI/***'
    --exclude 'images/***'
    --exclude 'isolinux/***'
  )
  local repo='archive.kernel.org::centos-vault/centos/8-stream'

  _print_section 'Syncing CentOS 8-Stream'
  mkdir -p "${REPO_DIR}/centos"
  rsync "${RSYNC_FLAGS[@]}" "${addl_flags[@]}" "${repo}" "${REPO_DIR}/centos/"
}

function sync_epel7() {
  local addl_flags=(
    --exclude '.~tmp~'
    --exclude '/*/SRPMS'
    --exclude '/*/ppc64/'
    --exclude '/*/ppc64le/'
    --exclude '/*/source/'
    --exclude '/*/state'
    --exclude '/*/aarch64/debug/'
    --exclude '/*/x86_64/debug/'
  )
  local repo="archives.fedoraproject.org::fedora-archive/epel/7"

  _print_section 'Syncing EPEL 7'
  mkdir -p "${REPO_DIR}/epel"
  rsync "${RSYNC_FLAGS[@]}" "${addl_flags[@]}" "${repo}" "${REPO_DIR}/epel/"
  rm -f "${REPO_DIR}/epel/7Server"
  ln -s 7 "${REPO_DIR}/epel/7Server"
}

function sync_epel8() {
  # x86_64 and arm64
  local addl_flags=(
    --exclude '.~tmp~'
    --exclude '/*/*/SRPMS'
    --exclude '/*/*/ppc64le/'
    --exclude '/*/*/s390x/'
    --exclude '/*/*/source/'
    --exclude '/*/*/aarch64/debug/'
    --exclude '/*/*/aarch64/drpms/'
    --exclude '/*/*/x86_64/debug/'
    --exclude '/*/*/x86_64/drpms/'
  )
  local repo="${EPEL_MIRROR}::epel/8"

  _print_section 'Syncing EPEL 8'
  mkdir -p "${REPO_DIR}/epel"
  rsync "${RSYNC_FLAGS[@]}" "${addl_flags[@]}" "${repo}" "${REPO_DIR}/epel/"
}

function sync_epel_next() {
  # x86_64 and arm64
  local addl_flags=(
    --exclude '.~tmp~'
    --exclude '/next/*/*/SRPMS'
    --exclude '/next/*/*/ppc64le/'
    --exclude '/next/*/*/source/'
    --exclude '/next/*/*/aarch64/debug/'
    --exclude '/next/*/*/aarch64/drpms/'
    --exclude '/next/*/*/x86_64/debug/'
    --exclude '/next/*/*/x86_64/drpms/'
    --exclude '/next/9/'
  )
  local repo="${EPEL_MIRROR}::epel/next"

  _print_section 'Syncing EPEL Next'
  mkdir -p "${REPO_DIR}/epel"
  rsync "${RSYNC_FLAGS[@]}" "${addl_flags[@]}" "${repo}" "${REPO_DIR}/epel/"
}

function _sync_ubuntu_pool {
  local repo="$1"
  local repo_dir="$2"
  local pool_dir="${repo_dir}/pool"
  local pool_name
  pool_name="$(basename "${repo_dir}")"
  declare -A ubuntu_pool
  local i=1
  local files="${repo_dir}/files.txt"
  local missing="${repo_dir}/missing.txt"
  local prune="${repo_dir}/prune.txt"
  truncate -s 0 "${files}" "${missing}" "${prune}"

  # Cache files we do have using a hash map
  pushd "${pool_dir}" &> /dev/null
  _print_section "Caching ${pool_name} pool"
  echo 'Packages:'
  while read -r line; do
    ubuntu_pool["${line#./}"]=1
    if [ $((${#ubuntu_pool[@]}%1024)) -eq 0 ]; then
      echo -en "\r  ${#ubuntu_pool[@]}"
    fi
  done < <(find . -type f)
  echo -e "\r  ${#ubuntu_pool[@]}"
  popd &> /dev/null

  # Process index files
  echo 'Processing index files'
  while read -r p; do
    zcat "${p}" | awk '/^Filename:/ {print $2}' >> "${files}"
  done < <(find "${repo_dir}/dists" -name Packages.gz)
  sed -i 's,^pool/,,' "${files}"
  sort -u "${files}" > tmp.$$
  mv tmp.$$ "${files}"

  # Remove unneeded packages
  sed -Ei '
    \,/b/binutils-,d
    \,/gcc-[0-9]+-cross-?,d
    \,/cross-gcc/,d
    \,/armel-cross,d
    \,/armhf-cross,d
    \,armel-cross/,d
    \,armhf-cross/,d
    \,/gcc-defaults-mips,d
    \,/gcc-defaults-ports/,d
    \,/gcc-h8300-hms/,d
    \,/gcc-mingw-w64/,d
    \,/linux-doc,d
    # Removes linux kernels we do not need
    \,^\w+/l/linux/,{
      /lowlatency/d
      /-source-/d
    }
    \,^\w+/l/linux-, {
      /lowlatency/d
      /-source-?/d
      /-aws/d
      /-azure/d
      /-bluefield/d
      /-dell300x/d
      /-gcp/d
      /-gke/d
      /-ibm/d
      /-iot/d
      /-intel/d
      /-oem/d
      /-oracle/d
      /-raspi/d
      /-riscv/d
    }
    # Remove all from pool/restricted/ except these
    \,^restricted/f/firmware-sof/,p
    \,^restricted/,d
    # Remove all nvidia except from pool/main/n/
    \,^main/n/nvidia,p
    /nvidia/d
    \,/f/fonts-android/,p
    /android/d
    /xilinx/d
  ' "${files}"

  # Cache files we should have using a hash map
  local num_files
  num_files="$(wc -l < "${files}")"
  echo 'Caching packages from index files'
  declare -A files_array
  i=0
  while read -r pkg; do
    i=$((i+1))
    if [ $((i%1024)) -eq 0 ]; then
      echo -en "\r  ${i}/${num_files}"
    fi
    files_array["${pkg}"]=1
  done < "${files}"
  echo -e "\r  ${i}/${num_files}"
  local delta="$((num_files-${#ubuntu_pool[@]}))"
  echo "Local pool files : ${#ubuntu_pool[@]}"
  echo "Remote pool files: ${num_files}"
  echo "Diff             : ${delta}"

  # Download packages
  local nmissing=0
  echo 'Finding missing files'
  i=0
  for pkg in "${!files_array[@]}"; do
    i=$((i+1))
    if [ $((i%1024)) -eq 0 ]; then
      echo -en "\r  ${i}/${#files_array[@]}"
    fi
    if [ -z "${ubuntu_pool["${pkg}"]}" ]; then
      echo "${pkg}" >> "${missing}"
      nmissing=$((nmissing+1))
    fi
  done
  echo -e "\r  ${i}/${#files_array[@]}"
  echo "Missing: ${nmissing} (to delete: $((nmissing-delta)))"
  if [ "${nmissing}" != 0 ]; then
    sort "${missing}" > tmp.$$
    mv tmp.$$ "${missing}"
    #shellcheck disable=SC2086
    rsync -Ltv ${DRY_RUN} ${SPARSE} --no-motd --ignore-missing-args \
      --files-from "${missing}" "${repo}/pool/" "${repo_dir}/pool/"
  fi

  # Prune packages not in files
  echo -e 'Pruning packages'
  local npruned=0
  i=0
  for pkg in "${!ubuntu_pool[@]}"; do
    echo "${pkg}" >> "${prune}"
  done
  while read -r pkg; do
    i=$((i+1))
    if [ $((i%1024)) -eq 0 ]; then
      echo -en "\r${i}/${#ubuntu_pool[@]}"
    fi
    if [ -z "${files_array["${pkg}"]}" ]; then
      echo -e "\r${i}/${#files_array[@]} ${repo_dir}/pool/${pkg}"
      [ -z "$DRY_RUN" ] && rm "${repo_dir}/pool/${pkg}"
      npruned=$((npruned+1))
    fi
  done < <(sort "${prune}")
  echo -e "\r${i}/${#ubuntu_pool[@]}"
  echo -e "Pruned ${npruned} packages"

  rm "${files}" "${missing}" "${prune}"
}

function sync_debian10_dists {
  local addl_flags=(
    --exclude '/*/*/*-arm*/'
    --exclude '/*/*/*-mips*/'
    --exclude '/*/*/*-ppc64el/'
    --exclude '/*/*/*-s390x/'
    --exclude '/*/*/debian-installer/'
    --exclude '/*/*/installer-*/'
    --exclude '/*/*/source/'
    --exclude '/*/*/Contents-source*/'
  )
  local index_repo='mirror.us.leaseweb.net::debian/dists/buster*'
  local dists_dir="${REPO_DIR}/debian/dists/"
  _print_section 'Syncing Debian 10 Index Files'
  mkdir -p "${dists_dir}"
  rsync "${RSYNC_FLAGS[@]}" "${addl_flags[@]}" "${index_repo}" "${dists_dir}"
}

function sync_debian_pool {
  local repo='mirror.us.leaseweb.net::debian'
  local repo_dir="${REPO_DIR}/debian"
  _print_section 'Syncing Debian Pool'
  # Ubuntu and debian use same layout, so this function works for debian also.
  _sync_ubuntu_pool "${repo}" "${repo_dir}"
}

function sync_ubuntu1204_dists {
  local addl_flags=(
    --exclude '/*/*/binary-arm*/'
    --exclude '/*/*/binary-powerpc/'
    --exclude '/*/*/binary-ppc64el/'
    --exclude '/*/*/debian-installer/'
    --exclude '/*/*/dist-upgrader-all/'
    --exclude '/*/*/installer-*/'
    --exclude '/*/*/uefi/'
  )
  local index_repo='old-releases.ubuntu.com::old-releases/ubuntu/dists/precise*'
  local dists_dir="${REPO_DIR}/ubuntu/dists/"
  _print_section 'Syncing Ubuntu 12.04 Index Files'
  mkdir -p "${dists_dir}"
  rsync "${RSYNC_FLAGS[@]}" "${addl_flags[@]}" "${index_repo}" "${dists_dir}"
}

function sync_ubuntu1804_dists {
  local addl_flags=(
    --exclude '/*/*/debian-installer/'
    --exclude '/*/*/dist-upgrader-all/'
    --exclude '/*/*/installer-*/'
    --exclude '/*/*/signed/'
    --exclude '/*/*/source/'
    --exclude '/*/*/uefi/'
  )
  local index_repo="${UBUNTU_MIRROR}::ubuntu/dists/bionic*"
  local dists_dir="${REPO_DIR}/ubuntu/dists/"
  _print_section 'Syncing Ubuntu 18.04 Index Files'
  mkdir -p "${dists_dir}"
  rsync "${RSYNC_FLAGS[@]}" "${addl_flags[@]}" "${index_repo}" "${dists_dir}"
}

function sync_ubuntu2004_dists {
  local addl_flags=(
    --exclude '/*/*/debian-installer/'
    --exclude '/*/*/installer-*/'
    --exclude '/*/*/signed/'
    --exclude '/*/*/source/'
    --exclude '/*/*/uefi/'
  )
  local index_repo="${UBUNTU_MIRROR}::ubuntu/dists/focal*"
  local dists_dir="${REPO_DIR}/ubuntu/dists/"
  _print_section 'Syncing Ubuntu 20.04 Index Files'
  mkdir -p "${dists_dir}"
  rsync "${RSYNC_FLAGS[@]}" "${addl_flags[@]}" "${index_repo}" "${dists_dir}"
}

function sync_ubuntu2204_dists {
  local addl_flags=(
    --exclude '/*/*/debian-installer/'
    --exclude '/*/*/installer-*/'
    --exclude '/*/*/signed/'
    --exclude '/*/*/source/'
    --exclude '/*/*/uefi/'
  )
  local index_repo="${UBUNTU_MIRROR}::ubuntu/dists/jammy*"
  local dists_dir="${REPO_DIR}/ubuntu/dists/"
  _print_section 'Syncing Ubuntu 22.04 Index Files'
  mkdir -p "${dists_dir}"
  rsync "${RSYNC_FLAGS[@]}" "${addl_flags[@]}" "${index_repo}" "${dists_dir}"
}

function sync_ubuntu_pool {
  local version="${1#ubuntu}"
  local repo="${UBUNTU_MIRROR}::ubuntu"
  local repo_dir="${REPO_DIR}/ubuntu"
  if [[ -z "${version}" || "${version}" -ge 1804 ]]; then
    _print_section 'Syncing Ubuntu Pool (Current)'
    _sync_ubuntu_pool "${repo}" "${repo_dir}"
  fi
  if [[ -z "${version}" || "${version}" -lt 1804 ]]; then
    repo='old-releases.ubuntu.com::old-releases/ubuntu'
    _print_section 'Syncing Ubuntu Pool (Old)'
    _sync_ubuntu_pool "${repo}" "${repo_dir}"
  fi
}

function sync_ubuntu2004_arm64_dists {
  local addl_flags=(
    --exclude '/**/*armhf*'
    --exclude '/**/*ppc64el*'
    --exclude '/**/*riscv64*'
    --exclude '/**/*s390x*'
    --exclude '/*/*/debian-installer/'
    --exclude '/*/*/dist-upgrader-all/'
    --exclude '/*/*/installer-*/'
    --exclude '/*/*/signed/'
    --exclude '/*/*/source/'
    --exclude '/*/*/uefi/'
  )
  local index_repo='ports.ubuntu.com::ubuntu-ports/dists/focal*'
  local dists_dir="${REPO_DIR}/ubuntu-ports/dists/"
  _print_section 'Syncing Ubuntu 20.04 ARM64 Index Files'
  mkdir -p "${dists_dir}"
  rsync "${RSYNC_FLAGS[@]}" "${addl_flags[@]}" "${index_repo}" "${dists_dir}"
}

function sync_ubuntu_arm64_pool {
  local repo='ports.ubuntu.com::ubuntu-ports'
  local repo_dir="${REPO_DIR}/ubuntu-ports"
  _print_section 'Syncing Ubuntu Ports Pool'
  _sync_ubuntu_pool "${repo}" "${repo_dir}"
}

function _sync_docker() {
  local ver=$1
  _print_section "Syncing Docker CE - CentOS ${ver}"
  local repo_dir="${REPO_DIR}/docker-ce-stable/linux/centos/${ver}/x86_64/stable/"
  mkdir -p "${repo_dir}"
  (
    cd "${repo_dir}"
    local conf=/etc/yum.conf
    if [ ! -f "${conf}" ]; then
      conf=/etc/dnf/dnf.conf
    fi
    if [ ! -f "${conf}" ]; then
      echo 'Missing dnf/yum conf file'
      exit 1
    fi
    cat "${conf}" "${REPO_DIR}/docker-ce.repo" > yum.conf
    sed -i "s/\$releasever/${ver}/" yum.conf
    reposync -mn --delete --norepopath -c yum.conf --repoid docker-ce-stable
    if [ "${ver}" = 7 ]; then
      createrepo_c --update --database --general-compress-type xz .
    else
      createrepo_c --update .
    fi
    rm -rf yum.conf cache
  )
}

function sync_docker_centos7() {
  _sync_docker 7
}

function sync_docker_centos8() {
  _sync_docker 8
}

function sync_redhat75() {
  source /etc/os-release
  if [ "${ID}" != rhel ]; then
    echo 'Not running on a redhat OS'
    exit 1
  fi

  local basearch
  basearch="$(python -c '
import yum
yb = yum.YumBase()
yb.doConfigSetup(init_plugins=False)
print yb.conf.yumvar["basearch"]
')"
  _print_section "Syncing RedHat 7.5 (${basearch})"

  # Do arch specific setup
  if [ "${basearch}" = x86_64 ]; then
    # Temporarily set to 7.5, this is restored at the end
    sudo subscription-manager release --set 7.5
    # Make sure repos we sync are enabled
    sudo subscription-manager repos \
      --enable rhel-server-rhscl-7-rpms \
      --enable rhel-7-server-devtools-rpms \
      --enable rhel-7-server-supplementary-rpms \
      --enable rhel-7-server-extras-rpms \
      --enable rhel-7-server-optional-rpms \
      --enable rhel-7-server-rpms
  else
    # Need to allow temporary write access for reposync
    sudo chmod 0644 /etc/pki/entitlement/*-key.pem
    # Make sure repos we sync are enabled
    sudo subscription-manager repos \
      --enable rhel-7-server-for-arm-64-rhscl-rpms \
      --enable rhel-7-server-for-arm-64-devtools-rpms \
      --enable rhel-7-for-arm-64-rpms \
      --enable rhel-7-for-arm-64-optional-rpms \
      --enable rhel-7-for-arm-64-extras-rpms
  fi
  sudo yum clean all
  sudo rm -rf /var/cache/yum
  sudo yum makecache fast

  # Need to sync one at a time to get the proper layout
  local repobase=
  local repo=
  local name=
  local url=
  local i=0
  local repofile="${REPO_DIR}/rhel75.${basearch}.repo"
  truncate -s 0 "${repofile}"
  while read -r line; do
    sudo -v  # keep sudo session alive
    i=$((i + 1))
    case ${i} in
      1) repo="${line}" ;;
      2) name="${line}" ;;
      3)
        i=0
        url="${line}"
        local path="${url#*cdn.redhat.com/}"
        path="$(eval echo "${path}")"
        repobase="${REPO_DIR}/rhel/${path}"

        echo "Synching repo: ${repo}"
        reposync -dlmn --norepopath -p "${repobase}" -r "${repo}"
        sudo -v  # keep sudo session alive

        if [[ "${repo}" = rhel-7-server-rpms || "${repo}" = rhel-7-for-arm-64-rpms ]]; then
          # For convenience since rhel doesn't provide
          cd "${repobase}/Packages/e"
          curl -O https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
          cd - &> /dev/null
        fi

        createrepo --update -g comps.xml "${repobase}"
        sudo -v  # keep sudo session alive
        cat >> "${repofile}" <<EOF
[${repo}]
name = ${name}
baseurl = ${url}
enabled = 1
gpgcheck = 0

EOF
        ;;
    esac
  done < <(sudo subscription-manager repos --list-enabled | awk -F':' '/Repo ID/ {
    for (i = 0; i < 3; i++) {
      gsub("/s+", "", $2)
      if ($3) {
        printf "http:%s\n", $3
      } else {
        print $2
      }
      getline
    }
  }')
  if [ "${basearch}" = x86_64 ]; then
    sudo subscription-manager release --unset
  else
    sudo chmod 0600 /etc/pki/entitlement/*-key.pem
  fi
  sudo yum clean all
  sudo rm -rf /var/cache/yum
}

function do_split() {
  local size="$1"

  if [[ -z "${size}" || "${size}" = dvd ]]; then
    size=939997952  # 5 chunks per dvd as max file size is limited
  elif [ "${size}" = cd ]; then
    size=99999000
  fi

  local chunks=$(( $(du -sb "${REPO_DIR}" | awk '{print $1}') / size ))
  echo "Creating ${chunks} chunks that are ${size} bytes each"
  tar -C "$(dirname "${REPO_DIR}")" -c "$(basename "${REPO_DIR}")" \
    | split --verbose -a 3 -d -b "${size}" - updates.tar.
}


##### MAIN #####

if [ "$(dirname "$0")" != . ]; then
  echo "run script from it's directory"
  exit 1
fi

# exit on ctrl+c
trap exit 1 SIGINT

if [ -z "$1" ]; then
  usage
fi

# Parse args
while [ -n "$1" ]; do
  case "$1" in
  all)
    sync_epel8
    sync_epel_next
    sync_docker_centos7
    sync_docker_centos8
    sync_ubuntu1804_dists
    sync_ubuntu2004_dists
    sync_ubuntu2204_dists
    sync_ubuntu_pool
    sync_ubuntu2004_arm64_dists
    sync_ubuntu_arm64_pool
    shift
    ;;
  centos)
    echo 'All supported centos repos are currently deprecated. Use an individual repo to sync it.'
    exit 1
    ;;
  debian)
    # Sync dists first, then pool. See ubuntu below for why.
    sync_debian10_dists
    sync_debian_pool
    shift
    ;;
  docker)
    sync_docker_centos7
    sync_docker_centos8
    shift
    ;;
  epel)
    sync_epel8
    sync_epel_next
    shift
    ;;
  ubuntu)
    # Need to sync index files first (dists), then the "pool" of packages. This
    # is due to ubuntu not storing packages in separate release specific
    # directories, which makes it impossible to prune packages without first
    # knowing all of the packages we should have.
    sync_ubuntu1804_dists
    sync_ubuntu2004_dists
    sync_ubuntu2204_dists
    sync_ubuntu_pool

    sync_ubuntu2004_arm64_dists
    sync_ubuntu_arm64_pool
    shift
    ;;
  # Individual repo sync options
  centos7|centos8|centos8_stream| \
  docker_centos7|docker_centos8| \
  epel7|epel8|epel_next| \
  redhat75)
    sync_"$1"
    shift
    ;;
  debian10| \
  ubuntu1204|ubuntu1804|ubuntu2004|ubuntu2204| \
  ubuntu2004_arm64)
    if [[ "$1" =~ arm64 ]]; then
      sync_"$1"_dists
      sync_ubuntu_arm64_pool
    elif [[ "$1" =~ ubuntu ]]; then
      sync_"$1"_dists
      sync_ubuntu_pool "$1"
    else
      sync_"$1"_dists
      sync_debian_pool
    fi
    shift
    ;;
  split)
    do_split "$2"
    shift 2
    ;;
  -n|--dry-run)
    # processed at start of script
    shift
    ;;
  *)
    echo "Unknown argument: $1"
    usage
    ;;
  esac
done

sync
if [ -z "${DRY_RUN}" ]; then
  echo
  echo 'Getting repo size. Update README.md with these.'
  du -sB G "${REPO_DIR}"
  echo
  du -sh "${REPO_DIR}"/*
fi
exit 0
