# repo-sync

Sync various repos for use on an offline network. This is not a 100% sync. Some
packages have been filtered out.

Size as of 02/01/2025
```bash
$ du -sB G repos/
1041G	repos/

$ du -sh repos/*
348G	repos/centos
224M	repos/docker-ce-stable
70G	    repos/epel
33G	    repos/rhel
476G	repos/ubuntu
116G	repos/ubuntu-ports
```

## Repos

The main script is `sync.bash` and uses either rsync or reposync depending on
the repo being synced. Run `./sync.bash --help` for more info.

- CentOS and Ubuntu only require `rsync`.
- Docker CE requires CentOS due to needing `createrepo` to create the metadata.
- Redhat 7.5 requires a licensed Redhat 7.

The repos are downloaded to the `repos/` folder. This folder is meant to be used
as the root of the webserver hosting the content. After the initial download,
checking for updates to the repos should be significantly faster, especially for
the repos that use rsync.

Using `rsync`, this folder can easily be synced with the offline server hosting
the repos. For example:
```bash
rsync -rtlpv --delete repos/ you@server:/srv/repos/
```

## Server

The `server/` folder contains config files for nginx and bind (named). The
location of where each of these files should go is provided in the server
README file.

### Nginx

The nginx config files expect the `repos/` folder to be rooted at `/opt/repos/`.
Should another location be desired, it should be trivial to update the files.

These config files, with the exception of redhat, allow for a client to
"just work" when installing packages hosted by the mirror.

#### EPEL

The EPEL repos by default are configured to use HTTPS, so creating a certificate
and private key pair for nginx to use will be required. Ideally this would be
signed by the CA on your private network that the clients are already configured
to trust. If using a self-signed cert, then yum/dnf can be configured to not do
any SSL verification using the following command:
```bash
# CentOS/Redhat 7
sudo yum install epel-release yum-config-manager
sudo yum-config-manager --save --setopt=epel.sslverify=0 > /dev/null

# CentOS/Redhat 8
sudo dnf install epel-release
sudo dnf config-manager --save --setopt=epel.sslverify=0 --setopt=epel-modular.sslverify=0
```

##### EPEL 7

The metalink format used by yum is a bit more strict than the format used by
dnf. Because of this, the metalink document that is returned by nginx for epel 7
needs to be updated everytime the epel 7 repo changes. Run
`/root/gen-metalink.sh` to update the necessary files.

### Bind

Example config files are provided for bind (aka named). These files have `TODO`s
in them for places that need to be modified.

The DNS config files expect nginx and named to be running on the same server.
If nginx and named are not on the same server, then some knowledge about named
config and zone files will be needed to make the necessary changes.

